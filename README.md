- [sampleJimiWeb](#orgfe07b4a)
  - [Screenshot](#org5cb8038)
  - [License](#org43885b5)


<a id="orgfe07b4a"></a>

# sampleJimiWeb

This is a sample website to practice basic HTML and CSS. Most importantly the compatibility of different screen sizes, from Desktops to phones.


<a id="org5cb8038"></a>

## Screenshot

![img](./Screenshot1.png "Preview of the Desktop look.")

![img](./Screenshot2.png "Preview of the Phone look.")


<a id="org43885b5"></a>

## License

GPL v3 - <https://www.gnu.org/licenses/gpl-3.0.en.html>